package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MyStepdefs {

    @Given("here put a given")
    public void herePutAGiven() {
        System.out.println("Here go the given");
    }

    @When("I add a when step")
    public void iAddAWhenStep() {
        System.out.println("Here go the when");
    }

    @Then("I have to assert something")
    public void iHaveToAssertSomething() {
        System.out.println("Here go the then");
    }
}
